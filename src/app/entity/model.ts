import {Brand} from './brand';
import {Modelyear} from './modelyear';
import {Version} from './version';

export class Model {

  modelid: number;
  model: string;
  brand: string;
  version: string;
  year: number;

  constructor() {
    this.modelid = 0;
  }

}

