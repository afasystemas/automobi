export class Version {

  private _id_version: number;
  private _version: string;

  constructor() {
    this._id_version = 0;
  }


  get id_version(): number {
    return this._id_version;
  }

  set id_version(value: number) {
    this._id_version = value;
  }

  get version(): string {
    return this._version;
  }

  set version(value: string) {
    this._version = value;
  }
}
