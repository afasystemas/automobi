import {ModuleWithProviders, NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import {VersionHomeComponent} from './pages/version/version-home/version-home.component';
import {BrandHomeComponent} from './pages/brand/brand-home/brand-home.component';
import {ModelyearHomeComponent} from './pages/modelyear/modelyear-home/modelyear-home.component';
import {ModelHomeComponent} from './pages/model/model-home/model-home.component';

@NgModule({
  exports: [ RouterModule ]
})
export class AppRoutesModule {}

export const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'versao', component: VersionHomeComponent},
  { path: 'marca', component: BrandHomeComponent},
  { path: 'ano', component: ModelyearHomeComponent},
  { path: 'modelo', component: ModelHomeComponent}
];
