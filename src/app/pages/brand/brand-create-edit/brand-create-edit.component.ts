import { Component, OnInit } from '@angular/core';
import {Version} from '../../../entity/version';
import {BrandCreateEditService} from './brand-create-edit.service';
import {Brand} from '../../../entity/brand';
import {VersionCreateEditService} from '../../version/version-create-edit/version-create-edit.service';
import {VersionListService} from '../../version/version-list/version-list.service';
import {BrandListService} from '../brand-list/brand-list.service';

@Component({
  selector: 'app-brand-create-edit',
  templateUrl: './brand-create-edit.component.html',
  styleUrls: ['./brand-create-edit.component.css']
})
export class BrandCreateEditComponent implements OnInit {

  brand: string;
  brandObject: Brand;
  alertEmpty: boolean;
  textAlert: String = 'Nome da marca não pode estar em branco!';

  constructor(private service: BrandCreateEditService, private editService: BrandListService) {
    this.brandObject = new Brand();
  }


  ngOnInit() {
    this.editService.brand.subscribe(
      data => this.brandObject = data

    );
  }

  public saveBrand() {

    console.log(this.brandObject)
    if (this.brandObject.name !== undefined && (this.brandObject.name.trim()) !== '') {

      this.service.saveBrandService(this.brandObject);
      this.brandObject = new Brand();
      this.alertEmpty = false;
    } else {
      this.alertEmpty = true;
    }

  }

}
