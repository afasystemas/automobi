import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandCreateEditComponent } from './brand-create-edit.component';

describe('BrandCreateEditComponent', () => {
  let component: BrandCreateEditComponent;
  let fixture: ComponentFixture<BrandCreateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandCreateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
