import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Brand} from '../../../entity/brand';
import {BrandListService} from '../brand-list/brand-list.service';

@Injectable()
export class BrandCreateEditService {



  url: String = 'http://localhost:3000';


  constructor(private http: HttpClient, private router: Router, private updateList: BrandListService) {
  }

  public saveBrandService(brand: Brand): any {

    const body = JSON.stringify(brand);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = {
      headers: headers
    };

    console.log(body);

    if (brand.id_brand === 0) {

      this.http.post(this.url + '/brand', body, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      })
        .subscribe((res: any) =>
          // console.log('requisição OK === ' + res[0]['id_version'])
          this.updateList.getAllBrands()), err => console.error(err);
    } else if (brand.id_brand > 0) {
      // console.log(' update  = ' + JSON.stringify(version));

      this.http.put(this.url + '/brand', body, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      })
        .subscribe((res: any) =>
          // console.log('requisição OK === ' + res[0]['id_version'])
          this.updateList.getAllBrands()), err => console.error(err);
    }
  }

}
