import { TestBed, inject } from '@angular/core/testing';

import { BrandCreateEditService } from './brand-create-edit.service';

describe('BrandCreateEditService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BrandCreateEditService]
    });
  });

  it('should be created', inject([BrandCreateEditService], (service: BrandCreateEditService) => {
    expect(service).toBeTruthy();
  }));
});
