import { TestBed, inject } from '@angular/core/testing';

import { BrandListService } from './brand-list.service';

describe('BrandListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BrandListService]
    });
  });

  it('should be created', inject([BrandListService], (service: BrandListService) => {
    expect(service).toBeTruthy();
  }));
});
