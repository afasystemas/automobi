import { Component, OnInit } from '@angular/core';
import {VersionListService} from '../../version/version-list/version-list.service';
import {Version} from '../../../entity/version';
import {BrandListService} from './brand-list.service';
import {Brand} from '../../../entity/brand';

@Component({
  selector: 'app-brand-list',
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.css']
})
export class BrandListComponent implements OnInit {

  brandList: Array<Brand>;
  columns: String[];


  constructor(private service: BrandListService) {
    this.service.getAllBrands();
  }

  ngOnInit() {
    this.service.brands.subscribe(
      data => this.brandList = data

    );
    this.service.columns.subscribe(
      data => this.columns = data

    );

  }

  public deleteBrand(id: string): void {

    if (window.confirm('Tem certeza que deseja excluir Marca com id ' + id + '?')) {
      this.service.deleteBrand(id);
    }

  }

  public editBrand(id: number): void {

    this.service.editBrand(this.brandList.filter(function(node) {
        return  node.id_brand === id;
      })[0]
    );




  }
