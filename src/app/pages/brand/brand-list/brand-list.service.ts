import {EventEmitter, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Version} from '../../../entity/version';
import {Brand} from '../../../entity/brand';

@Injectable()
export class BrandListService {

  brand: EventEmitter<Brand> = new EventEmitter<Brand>();
  brands: EventEmitter<any> = new EventEmitter<any>();
  columns: EventEmitter<any> = new EventEmitter<any>();
  url: String = 'http://localhost:3000';



  constructor(private http: HttpClient, private router: Router) {}

  public getAllBrands (): any {

    this.http.get(this.url + '/brands').subscribe(data => {
      this.columns.emit(Object.keys(data[0]));
      this.brands.emit(data);
      // console.log(data);
    });

  }

  public deleteBrand(id: string): void {

    const search = new URLSearchParams();
    search.set('id_brand', id);

    this.http.delete(this.url + '/brand/' + id , {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    })
      .subscribe((res: any) =>
          this.getAllBrands(),
        // console.log(res),
        error2 => console.log('erro de response === ' + error2));
  }


  public editBrand(brand: Brand): void {
    this.brand.emit(brand);

  }


}
