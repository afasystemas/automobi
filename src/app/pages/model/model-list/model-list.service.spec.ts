import { TestBed, inject } from '@angular/core/testing';

import { ModelListService } from './model-list.service';

describe('ModelListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModelListService]
    });
  });

  it('should be created', inject([ModelListService], (service: ModelListService) => {
    expect(service).toBeTruthy();
  }));
});
