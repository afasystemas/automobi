import { Component, OnInit } from '@angular/core';
import {ModelListService} from './model-list.service';
import {Model} from '../../../entity/model';
import {BrandListService} from '../../brand/brand-list/brand-list.service';
import {VersionListService} from '../../version/version-list/version-list.service';
import {ModelyearListService} from '../../modelyear/modelyear-list/modelyear-list.service';

@Component({
  selector: 'app-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.css']
})
export class ModelListComponent implements OnInit {


  modelList: Array<Model>;
  columns: String[];


  constructor(private service: ModelListService) {
    this.service.getAllModels();

  }

  ngOnInit() {
    this.service.models.subscribe(
      data => this.modelList = data

    );
    this.service.columns.subscribe(
      data => this.columns = data

    );
    // this.serviceList.getAllVersion();
  }

  public deleteModel(id: string): void {

    if (window.confirm('Tem certeza que deseja excluir modelo com id ' + id + '?')) {
      this.service.deleteModel(id);
    }

  }

  public editModel(id: number): void {

    this.service.editModel(this.modelList.filter(function(node) {
        return  node.modelid === id;
      })[0]
    );


  }

}
