import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, tap} from 'rxjs/operators';
import {Version} from '../../../entity/version';
import {Model} from '../../../entity/model';
import {Brand} from '../../../entity/brand';
import {Modelyear} from '../../../entity/modelyear';



@Injectable()
export class ModelListService {

  url: String = 'http://localhost:3000';
  model: EventEmitter<Model> = new EventEmitter<Model>();
  models: EventEmitter<any> = new EventEmitter<any>();
  listView: Array<string> = [];
  columns: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient, private router: Router) {}


  public getAllModels (): any {

    this.http.get(this.url + '/models').subscribe(data => {

      this.models.emit(data);
      // console.log(data);
      this.columns.emit(Object.keys(data[0]));
    });
  }

  public deleteModel(id: string): void {

    const search = new URLSearchParams();
    search.set('modelid', id);

    this.http.delete(this.url + '/model/' + id , {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    })
      .subscribe((res: any) =>
          this.getAllModels(),
        // console.log(res),
        error2 => console.log('erro de response === ' + error2));
  }


  public editModel(model: Model): void {

    this.model.emit(model);

  }

}
