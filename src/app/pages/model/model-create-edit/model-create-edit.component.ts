import { Component, OnInit } from '@angular/core';
import {SelectModule} from 'ng2-select';

import {ModelListService} from '../model-list/model-list.service';
import {ModelCreateEditService} from './model-create-edit.service';
import {Model} from '../../../entity/model';
import {Brand} from '../../../entity/brand';
import {BrandListService} from '../../brand/brand-list/brand-list.service';
import {ModelyearListService} from '../../modelyear/modelyear-list/modelyear-list.service';
import {VersionListService} from '../../version/version-list/version-list.service';
import {Version} from '../../../entity/version';
import {Modelyear} from '../../../entity/modelyear';

@Component({
  selector: 'app-model-create-edit',
  templateUrl: './model-create-edit.component.html',
  styleUrls: ['./model-create-edit.component.css']
})
export class ModelCreateEditComponent implements OnInit {

  modelObject: Model;
  alertModelEmpty: boolean;
  alertBrandEmpty: boolean;
  alertVersionEmpty: boolean;
  alertYearEmpty: boolean;
  textModelAlert: String = 'O modelo não pode estar em branco!';
  textBrandAlert: String = 'A Marca não pode estar em branco!';
  textVersionAlert: String = 'A versão não pode estar em branco!';
  textYearAlert: String = 'O ano não pode estar em branco!';
  labelBrand: String = 'Marca';
  labelVersion: String = 'Versão';
  labelYear: String = 'Ano';
  textButtonSave: String = 'Salvar';
  brandList: Array<Brand> = new Array<Brand>();
  versionList: Array<Version> = new Array<Version>();
  modelyearList: Array<Modelyear> = new Array<Modelyear>();

  constructor(private service: ModelCreateEditService,
              private editService: ModelListService,
              private brandService: BrandListService,
              private versionService: VersionListService,
              private modelYearService: ModelyearListService) {
    this.modelObject = new Model();
    this.brandService.getAllBrands();
    this.versionService.getAllVersion();
    this.modelYearService.getAllYears();

  }

  ngOnInit() {
    this.editService.model.subscribe(
      data => this.modelObject = data

    );
    this.brandService.brands.subscribe(
      data => this.brandList = data

    );
    this.versionService.versions.subscribe(
      data => this.versionList = data

    );
    this.modelYearService.years.subscribe(
      data => this.modelyearList = data
    );

  }

  public saveModel() {

    if (this.modelObject.version !== undefined &&
      this.modelObject.model !== undefined &&
      this.modelObject.brand !== undefined &&
      this.modelObject.year !== undefined &&
      (this.modelObject.version.trim()) !== '') {

      const brand = this.modelObject.brand;
      const id_brand = 0;

      this.brandList.forEach(function (node) {
          if (node.name === brand){
            id_brand = node.id_brand;
          }
      });

      const version = this.modelObject.version;
      const id_version = 0;

      this.versionList.forEach(function (node) {
        if (node.version === version){
          id_version = node.id_version;
        }
      });

      const year = this.modelObject.year;
      const model_year_id = 0;


      this.modelyearList.forEach(function (node) {
        if (node.year == year) {
          model_year_id = node.model_year_id;
        }
      });

      this.service.saveModelService(this.modelObject, id_brand,id_version,model_year_id);
      this.modelObject = new Model();
      this.alertModelEmpty = false;
      this.alertBrandEmpty = false;
      this.alertVersionEmpty = false;
      this.alertYearEmpty = false;
    } else {

      if (this.modelObject.model === undefined) {
        this.alertModelEmpty = true;
      } else {
        this.alertModelEmpty = false;
      }

      if (this.modelObject.brand === undefined) {
        this.alertBrandEmpty = true;
      } else {
        this.alertBrandEmpty = false;
      }

      if (this.modelObject.year === undefined) {
        this.alertYearEmpty = true;
      } else {
        this.alertYearEmpty = false;
      }

      if (this.modelObject.version === undefined) {
        this.alertVersionEmpty = true;
      } else {
        this.alertVersionEmpty = false;
      }
    }
  }
}
