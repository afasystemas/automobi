import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Model} from '../../../entity/model';
import {ModelListService} from '../model-list/model-list.service';

@Injectable()
export class ModelCreateEditService {

  url: String = 'http://localhost:3000';


  constructor(private http: HttpClient, private router: Router,
              private updateList: ModelListService) {
  }

  public saveModelService(model: Model, id_brand: number, id_version: number, model_year_id: number): any {

    console.log(model.modelid)
    console.log(id_brand)
    console.log(id_version)
    console.log(model_year_id)

    const dataModel = {
      modelid: model.modelid,
      id_brand: id_brand,
      name: model.model,
      model_year_id: model_year_id,
      id_version: id_version
    }

    const body = JSON.stringify(dataModel);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options =  {
      headers: headers
    };

    console.log(body);

    if (model.modelid === 0) {
      // console.log(' new  = ' +  JSON.stringify(mode));

      this.http.post(this.url + '/model', body, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      })
        .subscribe((res: any) =>
          // console.log('requisição OK === ' + res[0]['id_version'])
          this.updateList.getAllModels()), err => console.error(err);
    } else if (model.modelid > 0) {
      // console.log(' update  = ' +  JSON.stringify(model));

      this.http.put(this.url + '/model', body, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      })
        .subscribe((res: any) =>
          // console.log('requisição OK === ' + res[0]['id_version'])
          this.updateList.getAllModels()), err => console.error(err);
    }
  }


}
