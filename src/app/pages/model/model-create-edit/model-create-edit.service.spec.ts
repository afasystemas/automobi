import { TestBed, inject } from '@angular/core/testing';

import { ModelCreateEditService } from './model-create-edit.service';

describe('ModelCreateEditService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModelCreateEditService]
    });
  });

  it('should be created', inject([ModelCreateEditService], (service: ModelCreateEditService) => {
    expect(service).toBeTruthy();
  }));
});
