import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelCreateEditComponent } from './model-create-edit.component';

describe('ModelCreateEditComponent', () => {
  let component: ModelCreateEditComponent;
  let fixture: ComponentFixture<ModelCreateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelCreateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
