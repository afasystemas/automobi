import { TestBed, inject } from '@angular/core/testing';

import { VersionListService } from './version-list.service';

describe('VersionListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VersionListService]
    });
  });

  it('should be created', inject([VersionListService], (service: VersionListService) => {
    expect(service).toBeTruthy();
  }));
});
