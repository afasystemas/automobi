import {Component, EventEmitter, OnInit} from '@angular/core';
import {VersionListService} from './version-list.service';
import {Version} from '../../../entity/version';
import {VersionCreateEditComponent} from '../version-create-edit/version-create-edit.component';


@Component({
  selector: 'app-version-list',
  templateUrl: './version-list.component.html',
  styleUrls: ['./version-list.component.css']
})
export class VersionListComponent implements OnInit {

  versionList: Array<Version>;
  columns: String[];


  constructor(private service: VersionListService) {
    // this.versionList = this.service.getAllVersion();
    this.service.getAllVersion();
  }

  ngOnInit() {
    this.service.versions.subscribe(
      data => this.versionList = data

    );
    this.service.columns.subscribe(
      data => this.columns = data

    );
    // this.serviceList.getAllVersion();
  }

  public deleteVersion(id: string): void {

    if (window.confirm('Tem certeza que deseja excluir versão com id ' + id + '?')) {
      this.service.deleteVersion(id);
    }

  }

  public editVersion(id: number): void {

     this.service.editVersion(this.versionList.filter(function(node) {
              return  node.id_version === id;
            })[0]
     );


  }



}
