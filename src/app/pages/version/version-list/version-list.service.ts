import {EventEmitter, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/shareReplay';
import {Version} from '../../../entity/version';

@Injectable()
export class VersionListService {


  version: EventEmitter<Version> = new EventEmitter<Version>();
  versions: EventEmitter<any> = new EventEmitter<any>();
  columns: EventEmitter<any> = new EventEmitter<any>();
  url: String = 'http://localhost:3000';


  constructor(private http: HttpClient, private router: Router) {}

  public getAllVersion (): any {

    this.http.get(this.url + '/version').subscribe(data => {
      // console.log(data);

      this.columns.emit(Object.keys(data[0]));
      this.versions.emit(data);
             // console.log(this.versions);
    });

  }

  public deleteVersion(id: string): void {

    const search = new URLSearchParams();
    search.set('id_version', id);

    this.http.delete(this.url + '/version/' + id , {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    })
      .subscribe((res: any) =>
        this.getAllVersion(),
        // console.log(res),
        error2 => console.log('erro de response === ' + error2));
  }


  public editVersion(version: Version): void {
    this.version.emit(version);

  }
}
