import {Component, EventEmitter, OnInit, Input} from '@angular/core';
import {Version} from '../../../entity/version';
import {VersionCreateEditService} from './version-create-edit.service';
import {VersionListService} from '../version-list/version-list.service';


@Component({
  selector: 'app-version-create-edit',
  templateUrl: './version-create-edit.component.html',
  styleUrls: ['./version-create-edit.component.css']
})
export class VersionCreateEditComponent implements OnInit {

  versionObject: Version;
  alertEmpty: boolean;

  constructor(private service: VersionCreateEditService, private editService: VersionListService) {
    this.versionObject = new Version();
  }

  ngOnInit() {
    this.editService.version.subscribe(
      data => this.versionObject = data

    );
  }

  public saveVersion() {

    if (this.versionObject.version !== undefined && (this.versionObject.version.trim()) !== '') {

      this.service.saveVersionService(this.versionObject);
      this.versionObject = new Version();
      this.alertEmpty = false;
    } else {
      this.alertEmpty = true;
    }
  }

}
