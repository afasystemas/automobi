import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionCreateEditComponent } from './version-create-edit.component';

describe('VersionCreateEditComponent', () => {
  let component: VersionCreateEditComponent;
  let fixture: ComponentFixture<VersionCreateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionCreateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
