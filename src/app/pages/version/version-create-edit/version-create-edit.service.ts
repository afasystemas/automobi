import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Version} from '../../../entity/version';
import {catchError, tap} from 'rxjs/operators';
import {VersionListService} from '../version-list/version-list.service';

@Injectable()
export class VersionCreateEditService {


  url: String = 'http://localhost:3000';


  constructor(private http: HttpClient, private router: Router, private updateList: VersionListService) {
  }

  public saveVersionService(version: Version): any {

    const body = JSON.stringify(version);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options =  {
      headers: headers
    };

    // console.log(body);

    if (version.id_version === 0) {
      console.log(' new  = ' +  JSON.stringify(version));

      this.http.post(this.url + '/version', body, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      })
        .subscribe((res: any) =>
          // console.log('requisição OK === ' + res[0]['id_version'])
          this.updateList.getAllVersion()), err => console.error(err);
    } else if (version.id_version > 0) {
      console.log(' update  = ' +  JSON.stringify(version));

      this.http.put(this.url + '/version', body, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      })
        .subscribe((res: any) =>
          // console.log('requisição OK === ' + res[0]['id_version'])
          this.updateList.getAllVersion()), err => console.error(err);
    }
  }



}
