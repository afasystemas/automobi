import { TestBed, inject } from '@angular/core/testing';

import { VersionCreateEditService } from './version-create-edit.service';

describe('VerionCreateEditService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VersionCreateEditService]
    });
  });

  it('should be created', inject([VersionCreateEditService], (service: VersionCreateEditService) => {
    expect(service).toBeTruthy();
  }));
});
