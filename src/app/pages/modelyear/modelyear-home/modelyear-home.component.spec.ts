import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelyearHomeComponent } from './modelyear-home.component';

describe('ModelyearHomeComponent', () => {
  let component: ModelyearHomeComponent;
  let fixture: ComponentFixture<ModelyearHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelyearHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelyearHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
