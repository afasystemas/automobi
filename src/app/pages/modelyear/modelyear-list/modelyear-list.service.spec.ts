import { TestBed, inject } from '@angular/core/testing';

import { ModelyearListService } from './modelyear-list.service';

describe('ModelyearListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModelyearListService]
    });
  });

  it('should be created', inject([ModelyearListService], (service: ModelyearListService) => {
    expect(service).toBeTruthy();
  }));
});
