import {EventEmitter, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Modelyear} from '../../../entity/modelyear';
import {Version} from '../../../entity/version';

@Injectable()
export class ModelyearListService {


  year: EventEmitter<Modelyear> = new EventEmitter<Modelyear>();
  years: EventEmitter<any> = new EventEmitter<any>();
  columns: EventEmitter<any> = new EventEmitter<any>();
  url: String = 'http://localhost:3000';


  constructor(private http: HttpClient, private router: Router) {}

  public getAllYears (): any {

    this.http.get(this.url + '/years').subscribe(data => {
      // console.log(data);

      this.columns.emit(Object.keys(data[0]));
      this.years.emit(data);

    });
  }


  public deleteYear(id: string): void {

    const search = new URLSearchParams();
    search.set('model_year_id', id);

    this.http.delete(this.url + '/year/' + id , {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    })
      .subscribe((res: any) =>
          this.getAllYears(),
        // console.log(res),
        error2 => console.log('erro de response === ' + error2));
  }


  public editYear(year: Modelyear): void {
    this.year.emit(year);

  }

}
