import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelyearListComponent } from './modelyear-list.component';

describe('ModelyearListComponent', () => {
  let component: ModelyearListComponent;
  let fixture: ComponentFixture<ModelyearListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelyearListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelyearListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
