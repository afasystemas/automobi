import { Component, OnInit } from '@angular/core';
import {VersionListService} from '../../version/version-list/version-list.service';
import {Version} from '../../../entity/version';
import {ModelyearListService} from './modelyear-list.service';
import {Modelyear} from '../../../entity/modelyear';

@Component({
  selector: 'app-modelyear-list',
  templateUrl: './modelyear-list.component.html',
  styleUrls: ['./modelyear-list.component.css']
})
export class ModelyearListComponent implements OnInit {

  yearsList: Array<Modelyear>;
  columns: String[];


  constructor(private service: ModelyearListService) {
    this.service.getAllYears();
  }

  ngOnInit() {
    this.service.years.subscribe(
      data => this.yearsList = data

    );
    this.service.columns.subscribe(
      data => this.columns = data

    );

  }


  public deleteYear(id: string, year: number): void {

    if (window.confirm('Tem certeza que deseja excluir Ano ' + year + '?')) {
      this.service.deleteYear(id);
    }

  }

  public editYear(id: number): void {

    this.service.editYear(this.yearsList.filter(function(node) {
        return  node.model_year_id === id;
      })[0]
    );


  }




}
