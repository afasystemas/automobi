import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Version} from '../../../entity/version';
import {Router} from '@angular/router';
import {Modelyear} from '../../../entity/modelyear';
import {ModelyearListService} from '../modelyear-list/modelyear-list.service';

@Injectable()
export class ModelyearCreateEditService {


  url: String = 'http://localhost:3000';


  constructor(private http: HttpClient, private router: Router, private updateList: ModelyearListService) {
  }

  public saveYearService(year: Modelyear): any {


    const body = JSON.stringify(year);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = {
      headers: headers
    };

    // console.log(body);

    if (year.model_year_id === 0) {
      // console.log(' new  = ' + JSON.stringify(year));

      this.http.post(this.url + '/year', body, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      })
        .subscribe((res: any) =>
          // console.log('requisição OK === ' + res[0]['id_version'])
          this.updateList.getAllYears()), err => console.error(err);
    } else if (year.model_year_id > 0) {
      // console.log(' update  = ' + JSON.stringify(year));

      this.http.put(this.url + '/year', body, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      })
        .subscribe((res: any) =>
          // console.log('requisição OK === ' + res[0]['id_version'])
          this.updateList.getAllYears()), err => console.error(err);
    }
  }
}
