import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelyearCreateEditComponent } from './modelyear-create-edit.component';

describe('ModelyearCreateEditComponent', () => {
  let component: ModelyearCreateEditComponent;
  let fixture: ComponentFixture<ModelyearCreateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelyearCreateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelyearCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
