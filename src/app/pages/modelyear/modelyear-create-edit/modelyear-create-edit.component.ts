import { Component, OnInit } from '@angular/core';
import {Version} from '../../../entity/version';
import {VersionCreateEditService} from '../../version/version-create-edit/version-create-edit.service';
import {Modelyear} from '../../../entity/modelyear';
import {ModelyearCreateEditService} from './modelyear-create-edit.service';
import {VersionListService} from '../../version/version-list/version-list.service';
import {ModelyearListService} from '../modelyear-list/modelyear-list.service';

@Component({
  selector: 'app-modelyear-create-edit',
  templateUrl: './modelyear-create-edit.component.html',
  styleUrls: ['./modelyear-create-edit.component.css']
})
export class ModelyearCreateEditComponent implements OnInit {

  yearObject: Modelyear;
  alertEmpty: boolean;
  textAlert: String ;

  constructor(private service: ModelyearCreateEditService, private editService: ModelyearListService) {
    this.yearObject = new Modelyear();
  }

  ngOnInit() {
    this.editService.year.subscribe(
      data => this.yearObject = data

    );
  }

  public saveYear() {
    console.log(this.yearObject);

    if (this.yearObject.year !== undefined &&
      this.yearObject.year > 1920
      && this.yearObject.year < 2050) {
      console.log('sanvando');
      this.service.saveYearService(this.yearObject);
      this.yearObject = new Modelyear();
      this.alertEmpty = false;
    } else if (this.yearObject.year < 1920) {
      console.log('menor 1 1920');
      this.textAlert = 'Ano deve ser maior que 1920!';
      this.alertEmpty = true;
    } else if (this.yearObject.year > 2050) {
      console.log('maior 1 2050');
      this.textAlert = 'Ano deve ser menor que 2050!';
      this.alertEmpty = true;
    } else if (this.yearObject.year == null) {
      console.log('em branco ');
      this.textAlert = 'Ano não pode estar em branco!';
      this.alertEmpty = true;
    }

  }


}
