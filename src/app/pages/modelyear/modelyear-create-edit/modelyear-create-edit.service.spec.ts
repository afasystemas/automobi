import { TestBed, inject } from '@angular/core/testing';

import { ModelyearCreateEditService } from './modelyear-create-edit.service';

describe('ModelyearCreateEditService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModelyearCreateEditService]
    });
  });

  it('should be created', inject([ModelyearCreateEditService], (service: ModelyearCreateEditService) => {
    expect(service).toBeTruthy();
  }));
});
