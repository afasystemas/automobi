import { Component, OnInit } from '@angular/core';
import {} from '../../../assets/Image/'
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  logoPath: String = '../../../assets/Image/logo.png';
  iconPath: String = '../../../assets/Image/icon.png';
  constructor() { }

  ngOnInit() {
  }

}
