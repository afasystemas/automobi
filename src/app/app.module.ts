import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// import { TooltipModule } from 'ngx-bootstrap/tooltip';
// import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { ModelListComponent } from './pages/model/model-list/model-list.component';
import { MenuComponent } from './pages/menu/menu.component';
import { ModelCreateEditComponent } from './pages/model/model-create-edit/model-create-edit.component';
import { BrandListComponent } from './pages/brand/brand-list/brand-list.component';
import { BrandCreateEditComponent } from './pages/brand/brand-create-edit/brand-create-edit.component';
import { VersionListComponent } from './pages/version/version-list/version-list.component';
import { VersionCreateEditComponent } from './pages/version/version-create-edit/version-create-edit.component';
import { ModelyearListComponent } from './pages/modelyear/modelyear-list/modelyear-list.component';
import { ModelyearCreateEditComponent } from './pages/modelyear/modelyear-create-edit/modelyear-create-edit.component';
import { HomeComponent } from './pages/home/home.component';
import {AppRoutesModule, routes} from './app.routes';
import {RouterModule, Routes} from '@angular/router';

import { VersionHomeComponent } from './pages/version/version-home/version-home.component';
import { BrandHomeComponent } from './pages/brand/brand-home/brand-home.component';
import { ModelHomeComponent } from './pages/model/model-home/model-home.component';
import { ModelyearHomeComponent } from './pages/modelyear/modelyear-home/modelyear-home.component';
import { VersionListService } from './pages/version/version-list/version-list.service';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {VersionCreateEditService} from './pages/version/version-create-edit/version-create-edit.service';
import {BrandListService} from './pages/brand/brand-list/brand-list.service';
import {BrandCreateEditService} from './pages/brand/brand-create-edit/brand-create-edit.service';
import {ModelyearCreateEditService} from './pages/modelyear/modelyear-create-edit/modelyear-create-edit.service';
import {ModelyearListService} from './pages/modelyear/modelyear-list/modelyear-list.service';
import {ModelListService} from './pages/model/model-list/model-list.service';
import {ModelCreateEditService} from './pages/model/model-create-edit/model-create-edit.service';

@NgModule({
  declarations: [
    AppComponent,
    ModelListComponent,
    MenuComponent,
    ModelCreateEditComponent,
    BrandListComponent,
    BrandCreateEditComponent,
    VersionListComponent,
    VersionCreateEditComponent,
    ModelyearListComponent,
    ModelyearCreateEditComponent,
    HomeComponent,
    VersionHomeComponent,
    BrandHomeComponent,
    ModelHomeComponent,
    ModelyearHomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutesModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule
  ],
  providers: [VersionListService,
    VersionCreateEditService,
    BrandListService,
    BrandCreateEditService,
    ModelyearCreateEditService,
    ModelyearListService,
    ModelListService,
    ModelCreateEditService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
